#!/bin/sh
#
# COPYRIGHT & LICENSE
#
# Copyright 2017 Easy Connect AS
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# AUTHORS
#
# Knut Arne Bjørndal <knut.arne.bjorndal@easyconnect.no>
#

set -u;

_run_invasive() {
    if [ -n "$dryrun" ]; then
	echo "DRY-RUN: " "$@"
    else
	echo "Executing:" "$@"
	"$@"
    fi
}

verify_cluster() {
    if hcheck -L --no-simulation --machine-readable | grep ^HCHECK_INIT_CLUSTER_OFFLINE_PRI= | grep -v =0 >/dev/null; then
	echo "hcheck reports instances with offline or drained primary nodes, aborting";
	return 1
    fi
    _run_invasive gnt-cluster verify || return $?
}

check_cluster_nplus1() {
    hcheck -L --no-simulation --machine-readable | grep ^HCHECK_INIT_CLUSTER_N1_FAIL= | grep "=0" > /dev/null
}

check_balanced() {
    for nodegroup in $(gnt-group list --no-headers -o uuid); do
	if [ "$(hbal -L -G "$nodegroup" | egrep '^Solution length=.*' | cut -d= -f2)" -gt 0 ]; then
	    return 1
	fi
    done
    # If we get here all the node groups are balanced
    return 0
}

list_nodes() {
    if [ $# -gt 0 ]; then
	gnt-node list --no-headers -o name -F "$@"
    else
	gnt-node list --no-headers -o name
    fi
}

tag_nodes() {
    tag="$1"; shift;
    for n in "$@"; do
	_run_invasive gnt-node add-tags "$n" "$tag" || return $?
    done
}

untag_nodes() {
    tag="$1"; shift;
    for n in "$@"; do
	_run_invasive gnt-node remove-tags "$n" "$tag" || return $?
    done
}

get_rebootgroup() {
    hroller -L --node-tags "$needs_maintenance_tag" --one-step-only --no-headers
}

abort_if_master_in_rebootgroup() {
    rebootgroup="$*"
    master=$(gnt-cluster getmaster) || return $?
    for n in $rebootgroup; do
	if [ "$n" = "$master" ]; then
	    echo "Master ($master) is in next rebootgroup, please failover to a different master candidate and re-run this script there to continue";
	    return 1
	fi
    done

    return 0;
}

set_node_drained() {
    _run_invasive gnt-node modify -D yes "$1"
}

set_node_offline() {
    _run_invasive gnt-node modify -O yes "$1"
}

unset_node_offline() {
    _run_invasive gnt-node modify -O no "$1"
}

balance_cluster() {
    for nodegroup in $(gnt-group list --no-headers -o uuid); do
	_run_invasive hbal -L -G "$nodegroup" -X "$@" || return $?
    done
}

# shellcheck disable=SC2086
tag() {
    nodes=$(list_nodes "$@") || return $?;
    tag_nodes "$needs_maintenance_tag" $nodes || return $?;

    echo "Nodes with $needs_maintenance_tag tag:"
    list_nodes "'$needs_maintenance_tag' in tags";
}

# shellcheck disable=SC2086
next_tag_maintenance_group() {
    verify_cluster || return $?;
    
    rebootgroup=$(get_rebootgroup) || return $?;
    echo "Next maintenance group:";
    echo "$rebootgroup";

    abort_if_master_in_rebootgroup $rebootgroup || return $?;

    tag_nodes "$next_maintenance_tag" $rebootgroup || return $?;
}

next_evacuate_nodes() {
    nodes="$(list_nodes "'$next_maintenance_tag' in tags" )" || return $?;

    for n in $nodes; do
	set_node_drained "$n" || return $?

	_run_invasive gnt-node migrate -f "$n" || return $?
    done

    gnt-cluster verify --no-nplus1-mem || return $?

    for n in $nodes; do
	set_node_offline "$n" || return $?;
	tag_nodes "$in_maintenance_tag" "$n" || return $?;
	untag_nodes "$next_maintenance_tag" "$n" || return $?;
    done

    echo "Nodes drained and ready for maintenance:";
    echo "$nodes";
}

interactive_confirm_finished_maintenance() {
    echo "Press enter when the following nodes are online again after maintenance:";
    list_nodes "'$in_maintenance_tag' in tags"
    read -r _;
}

next_clear_offline() {
    in_maintenance="$(list_nodes "'$in_maintenance_tag' in tags")" || return $?;

    for n in $in_maintenance; do
	if ping -c 1 "$n" > /dev/null; then
	    unset_node_offline "$n" || return $?;
	    untag_nodes "$in_maintenance_tag" "$n" || return $?;
	    untag_nodes "$needs_maintenance_tag" "$n" || return $?;
	else
	    echo "$n not reachable, leaving in offline state" >&2;
	fi
    done

    _run_invasive ganeti-watcher || return $?;
    balance_cluster --no-disk-moves || return $?;
    check_cluster_nplus1 || {
	echo "hbal --no-disk-moves did not leave the cluster N+1, trying again";
	balance_cluster || return $?;
    }
}

next() {
    test $# -eq 0 || {
	echo "next (and run) does not take any arguments" >&2
	return 64
    }

    if [ "$(list_nodes "'$next_maintenance_tag' in tags" | wc -l)" -gt 0 ]; then
	next_evacuate_nodes || return $?;
    elif [ "$(list_nodes "'$in_maintenance_tag' in tags" | wc -l)" -gt 0 ]; then
	interactive_confirm_finished_maintenance || return $?;
	next_clear_offline || return $?;
    elif [ "$(list_nodes "'$needs_maintenance_tag' in tags" | wc -l)" -gt 0 ]; then
	echo "Finding the next maintenance group";
	next_tag_maintenance_group || return $?
    elif ! check_balanced; then
	balance_cluster || return $?;
    else
	echo "nothing more to do";
	finished=1;
    fi
}

run() {
    while [ ! "$finished" ]; do
	next "$@" || return $?
    done
}

usage() {
    if [ $# -eq 2 ]; then
	echo "$1" >&2
	shift;
    fi

    cat <<EOF
Usage: $0 [options] COMMAND

COMMANDS:

tag [filter]
	Tag all nodes (matching optional filter) as needing maintenance

next
	Does the first of these steps that are applicable:
	* If there are nodes scheduled for maintenance: Drain them and mark them as offline
	* If there are nodes down for maintenance: Mark them as online after asking for confirmation
	* If there are nodes tagged as needing maintenance: Find a group using hroller and schedule them for maintenance
	* If the cluster is unbalanced: Balance it

run
	Runs "next" in a loop until there is nothing more to do

OPTIONS:
	--needs-maintenance-tag

		Tag used to indicate that a node needs
		maintenance. Defaults to needsreboot

	--in-maintenance-tag

		Tag used to indicate that a node is currently
		undergoing maintenance. Defaults to in_maintenance

	--next-maintenance-tag

		Tag used to indicate the next set of nodes scheduled
		for maintenance. Defaults to next_maintenance_group

	--dry-run

		Don't actually run any commands that change the state
		of the cluster

EOF

    if [ $# -eq 1 ]; then
	exit "$1"
    fi
}

needs_maintenance_tag=needsreboot
next_maintenance_tag=next_maintenance_group
in_maintenance_tag=in_maintenance
dryrun=''

finished=''

while [ $# -gt 0 ]; do
    case "$1" in
	--needs-maintenance-tag|-t)
	    needs_maintenance_tag="$2";
	    shift 2;
	    ;;
	--in-maintenance-tag)
	    in_maintenance_tag="$2";
	    shift 2;
	    ;;
	--next-maintenance-tag)
	    next_maintenance_tag="$2";
	    shift 2;
	    ;;
	--dry-run|--dryrun|-n)
	    dryrun=1
	    shift 1;
	    ;;
	-h|--help)
	    usage 0;
	    ;;
	-*)
	    usage "Unknown option $1" 64;
	    ;;
	*)
	    break
	    ;;
    esac
done

test $# -eq 0 && usage "Missing command" 64

command=''
case "$1" in
    tag) command='tag'; shift ;;
    next) command='next'; shift ;;
    run) command='run'; shift ;;
    *)
	usage "Unknown command $1" 64;
	;;
esac

"$command" "$@" || exit $?
